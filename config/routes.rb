Rails.application.routes.draw do
  namespace :private do
    resources :locations, only: [:show]
    resources :target_groups, only: [:show]
    resource :evaluate_target, only: [:create]
  end

  resources :locations, only: [:show]
  resources :target_groups, only: [:show]
end
