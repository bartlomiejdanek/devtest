class Private::EvaluateTargetsController < Private::ApplicationController
  def create
    render json: permitted_params
  end

  private

  def permitted_params
    params.permit(:country_code, :target_group_id, locations: [:id, :panel_size])
  end
end
