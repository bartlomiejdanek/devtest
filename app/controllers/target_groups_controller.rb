class TargetGroupsController < ApplicationController
  def show
    target_groups = TargetGroup.where(panel_provider: current_panel_provider)

    render json: target_groups
  end

  def current_panel_provider
    PanelProvider.find(1)
  end
end
