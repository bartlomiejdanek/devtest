class LocationsController < ApplicationController
  def show
    countries = Country.where(country_code: params[:id], panel_provider: current_panel_provider.id)
    locations = Location.joins(:location_groups).where(location_groups: { country_id: countries })

    render json: locations
  end

  def current_panel_provider
    PanelProvider.find(1)
  end
end
