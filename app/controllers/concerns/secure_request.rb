module SecureRequest
  extend ActiveSupport::Concern

  included do
    before_action :secure_request!

    private

    def secure_request!
      unless request.env['TOKEN'] == 'secret'
        render json: { missed: :token }
      end
    end
  end
end
