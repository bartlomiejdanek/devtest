require 'json'
require 'nokogiri'

# The price should be based on how many letters "a" can you find on this site http://time.com divided by 100
time_com = File.read('spec/fixtures/time.html')
puts time_com.count("a") / 100

# The price should be based on how many arrays with more than 10 elements you can find in this search result
lord = JSON.parse(File.read('spec/fixtures/lord.json'))
@counter = 0

def count_big_arrays(hash)
  hash.each do |(key, value)|
    if value.is_a?(Array) && value.size > 10
      @counter += 1
    end
    if value.is_a?(Hash)
      count_big_arrays(value)
    elsif value.is_a?(Array)
      value.each do |subvalue|
        count_big_arrays(subvalue) if subvalue.is_a?(Hash)
      end
    end
  end
end

count_big_arrays(lord)

puts @counter

# The price should be based on how many html nodes can you find on this site http://time.com divided by 100
require 'enumerator'
time_doc = Nokogiri.HTML(time_com)
nodes = time_doc.enum_for(:traverse)
puts nodes.count
