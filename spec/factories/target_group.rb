FactoryGirl.define do
  factory :target_group do
    name { FFaker::Address.city }
    secret_code { SecureRandom.hex }
  end
end
