FactoryGirl.define do
  factory :location do
    name { FFaker::AddressUS.state }
    secret_code { FFaker::AddressUS.zip_code }
  end
end
