FactoryGirl.define do
  factory :panel_provider do
    code { FFaker::Random.rand(100_000) }
  end
end
