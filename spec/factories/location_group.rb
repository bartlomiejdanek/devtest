FactoryGirl.define do
  factory :location_group do
    name { FFaker::AddressUS.state }
  end
end
