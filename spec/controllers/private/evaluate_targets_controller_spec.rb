require 'rails_helper'

RSpec.describe Private::EvaluateTargetsController do
  describe '#create' do
    it 'returns a permitted params' do
      request.env['TOKEN'] = 'secret'
      post :create, country_code: 'pl', target_group_id: 1, locations: [ {id: 1, panel_size: 2000}]

      response_body = JSON.parse(response.body)
      expect(response_body).to eq({
        'country_code' => 'pl',
        'target_group_id' => '1',
        'locations' => [ {'id' => '1', 'panel_size' => '2000'} ]
      })
    end

    context 'without a token' do
      it 'returns an error' do
        post :create
        response_body = JSON.parse(response.body)
        expect(response_body).to eq('missed' => 'token')
      end
    end
  end
end
