require 'rails_helper'

RSpec.describe LocationsController do
  describe '#show' do
    it 'returns locations which belong to a country code' do
      country_1 = FactoryGirl.create(:country, country_code: 'pl')
      country_2 = FactoryGirl.create(:country, country_code: 'pl')
      country_3 = FactoryGirl.create(:country, country_code: 'sv', panel_provider: country_1.panel_provider)
      country_4 = FactoryGirl.create(:country, country_code: 'de')

      location_group_1 = FactoryGirl.create(:location_group, country: country_1, panel_provider: country_1.panel_provider)
      locations_1 = FactoryGirl.create_list(:location, 5)

      locations_1.each do |location_1|
        location_group_1.locations_location_groups.create(location_id: location_1.id)
      end

      location_group_2 = FactoryGirl.create(:location_group, country: country_2)
      locations_2 = FactoryGirl.create_list(:location, 5)

      locations_2.each do |location_2|
        location_group_2.locations_location_groups.create(location_id: location_2.id)
      end

      allow(controller).to receive(:current_panel_provider) { country_1.panel_provider }
      get :show, id: 'pl'

      response_body = JSON.parse(response.body)
      expect(response_body).to match_array(locations_1.map(&:as_json))
    end
  end
end
