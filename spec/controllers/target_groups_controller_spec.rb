require 'rails_helper'

RSpec.describe TargetGroupsController do
  describe '#show' do
    it 'returns target groups which belong to a country code' do
      country_1 = FactoryGirl.create(:country, country_code: 'pl')
      country_2 = FactoryGirl.create(:country, country_code: 'pl')
      country_3 = FactoryGirl.create(:country, country_code: 'sv', panel_provider: country_1.panel_provider)
      country_4 = FactoryGirl.create(:country, country_code: 'de')

      allow(controller).to receive(:current_panel_provider) { country_1.panel_provider }

      blank_target_group = FactoryGirl.create(:target_group)
      target_group_pl = FactoryGirl.create(:target_group, panel_provider: country_1.panel_provider)
      not_a_target_group_pl = FactoryGirl.create(:target_group, panel_provider: country_2.panel_provider)

      get :show, id: 'pl'

      response_body = JSON.parse(response.body)
      expect(response_body).to match_array([target_group_pl.as_json])
    end
  end
end
