# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


3.times do
  FactoryGirl.create(:country)
end

20.times do
  FactoryGirl.create(:location)
end

@panel_providers = PanelProvider.all

3.times do |idx|
  FactoryGirl.create(:location_group, panel_provider: @panel_providers[idx], country: @panel_providers[idx].countries.first )
end

@panel_providers.first.tap do |panel_provider|
  FactoryGirl.create(:location_group, panel_provider: panel_provider, country: panel_provider.countries.first)
end

def create_target_group(root, level_of_nest = 1)
  FactoryGirl.create(:target_group, parent_id: root.id, panel_provider: @panel_providers.first)
  3.times do |idx|
    leaf = FactoryGirl.create(:target_group, parent_id: root.id, panel_provider: @panel_providers[idx])
    if level_of_nest <= 3
      create_target_group(leaf, level_of_nest + 1)
    end
  end
end

root = FactoryGirl.create(:target_group)
create_target_group(root)
