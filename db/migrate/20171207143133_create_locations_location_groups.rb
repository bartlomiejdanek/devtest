class CreateLocationsLocationGroups < ActiveRecord::Migration
  def change
    create_table :locations_location_groups, id: false do |t|
      t.integer :location_id
      t.integer :location_group_id
    end
  end
end
