class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name
      t.integer :external_id, index: true
      t.string :secret_code
    end
  end
end
