class CreateLocationGroups < ActiveRecord::Migration
  def change
    create_table :location_groups do |t|
      t.string :name
      t.integer :country_id, index: true
      t.integer :panel_provider_id, index: true
    end
  end
end
