class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.string :country_code, null: false
      t.integer :panel_provider_id, index: true
    end
  end
end
