class CreateTargetGroup < ActiveRecord::Migration
  def change
    create_table :target_groups do |t|
      t.string :name
      t.integer :external_id, index: true
      t.integer :parent_id, index: true
      t.string :secret_code
      t.integer :panel_provider_id, index: true
    end
  end
end
